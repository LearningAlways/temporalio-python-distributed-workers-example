# Separation of Requirements Example

This is an example of how to separate requirements for different workers and the workflow.

This example has a workflow which will use two workers (A and B) which have different requirements (for the example pydantic 1.0 and 2.0).

Each worker can be started and run independently and oblivious to the workflow.

## Requirements

Install poetry (or manage packages with pip or other package manager)


Install and run the temporal server

```bash
brew install temporalio
```

Start the temporal server
```bash 
temporal server start-dev --port 1234
```

Start Worker A
```bash
cd workerA
poetry install

poetry run python run_worker_a.py
```

Start Worker B
```bash
cd workerB
poetry install

poetry run python run_worker_b.py
```

The workflow will be started and run independently and only must know the task-queue and the name of the activity definitions for the workers.
```bash
cd workflow
poetry install

poetry run python run_worker.py
```

The workflow could be started from a different machine or environment, and the workers could be started in different machines or environments.
```bash
cd workflow
poetry install

poetry run python run_workflow.py
```