import asyncio

from temporalio.client import Client
from temporalio.worker import Worker

from workflow import WorkflowDefinitionClass

async def main() -> None:
    client: Client = await Client.connect("localhost:1234", namespace="default")
    # Run the worker
    print("Hosting worker for WorkflowDefinitionClass")

    task_queues = ["task-queue-a", "task-queue-b"]
    tasks = []
    # Start a worker for each task queue
    for task_queue in task_queues:
        worker: Worker = Worker(
            client,
            task_queue=task_queue,
            workflows=[WorkflowDefinitionClass],
        )
        tasks.append(asyncio.create_task(worker.run()))

    # Wait for all workers to finish
    await asyncio.gather(*tasks)


if __name__ == "__main__":
    asyncio.run(main())