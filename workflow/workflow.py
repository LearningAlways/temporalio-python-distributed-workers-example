from datetime import timedelta
from temporalio import workflow
import logging

logging.basicConfig(level=logging.DEBUG)

START_TO_CLOSE_TIMEOUT = 10

# Import activity, passing it through the sandbox without reloading the module
with workflow.unsafe.imports_passed_through():
    pass
    # from workerA.activity_a import activity_a_function
    # from workerB.activity_b import activity_b_function

@workflow.defn
class WorkflowDefinitionClass:
    @workflow.run
    async def run(self, name: str) -> str:
        result_a = await workflow.execute_activity(
            "activity_a_function", 
            name, 
            start_to_close_timeout=timedelta(seconds=START_TO_CLOSE_TIMEOUT),
            task_queue="task-queue-a"
        )
        workflow.logger.info(f"Activity A returned: {result_a}")
        result_b = await workflow.execute_activity(
            "activity_b_function", 
            name, 
            start_to_close_timeout=timedelta(seconds=START_TO_CLOSE_TIMEOUT),
            task_queue="task-queue-b"
        )
        workflow.logger.info(f"Activity B returned: {result_b}")

        return f"{result_a} {result_b}"