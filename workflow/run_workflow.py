import asyncio
import traceback

from temporalio.client import Client, WorkflowFailureError

from workflow import WorkflowDefinitionClass
import uuid

async def main() -> None:
    # Create client connected to server at the given address
    client: Client = await Client.connect("localhost:1234")

    try:
        N = 3
        tasks = []
        # Make N tasks
        for i in range(N):
            tasks.append(
                asyncio.create_task(
                    client.execute_workflow(
                        WorkflowDefinitionClass.run,
                        f"hi from task {i}",
                        id=f"id-{uuid.uuid4()}",
                        task_queue="task-queue-a",
                    )
                )
            )
        results = await asyncio.gather(*tasks)
        for r in results:
            print(r)

    except WorkflowFailureError:
        print("Got expected exception: ", traceback.format_exc())


if __name__ == "__main__":
    asyncio.run(main())