from temporalio import activity
import pydantic
import asyncio
import logging

@activity.defn
async def activity_b_function(name: str) -> str:
    activity.logger.info(f"Activity B received name: {name} doing work with pydantic version {pydantic.__version__}")
    await asyncio.sleep(3)
    return f"Hello, {name}! I am activity b."