import asyncio
import logging
from temporalio.client import Client
from temporalio.worker import Worker

from activity_b import activity_b_function
logging.basicConfig(level=logging.INFO)

async def main() -> None:
    client: Client = await Client.connect("localhost:1234", namespace="default")
    # Run the worker
    logging.info("Running worker B")
    worker: Worker = Worker(
        client,
        task_queue="task-queue-b",
        activities=[activity_b_function],
    )
    await worker.run()


if __name__ == "__main__":
    asyncio.run(main())