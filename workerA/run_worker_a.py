import logging
import asyncio

from temporalio.client import Client
from temporalio.worker import Worker

from activity_a import activity_a_function

logging.basicConfig(level=logging.INFO)

async def main() -> None:
    client: Client = await Client.connect("localhost:1234", namespace="default")
    # Run the worker
    logging.info("Running worker A")
    worker: Worker = Worker(
        client,
        task_queue="task-queue-a",
        activities=[activity_a_function],
    )
    await worker.run()


if __name__ == "__main__":
    asyncio.run(main())