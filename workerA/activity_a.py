from temporalio import activity
import asyncio 
import logging
import pydantic


@activity.defn
async def activity_a_function(name: str) -> str:
    activity.logger.info(f"Activity A received name: {name} doing work with pydantic version {pydantic.VERSION}")
    await asyncio.sleep(5)
    return f"Hello, {name}! I am activity A."